#ifndef GAME_FUNCTIONS_H_
#define GAME_FUNCTIONS_H_

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <stdint.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

#define MAX_PLANETS 8
#define MAX_SPACESHIPS 9

typedef struct
{
    uint16_t planet_id;
    uint16_t abscissa;
    uint16_t ordinate;
    int8_t ship_id;
    bool saved;
} Planet;

typedef struct
{
    uint8_t team;
    uint8_t ship_id;
    uint16_t abscissa;
    uint16_t ordinate;
    bool broken;
} Spaceship;

typedef struct
{
    unsigned short abscissa;
    unsigned short ordinate;
} BaseStation;

typedef struct
{
    Planet planets[MAX_PLANETS];
    Spaceship my_spaceships[MAX_SPACESHIPS];
    Spaceship enemy_spaceships[MAX_SPACESHIPS * 3];
    BaseStation base_station;
} RadarData;

void parse_data(const char *data, RadarData *radarData);
int atoi(const char *str);
uint16_t calcul_angle(int16_t x1, int16_t y1, int16_t x2, int16_t y2);
uint16_t calcul_distance(int16_t x1, int16_t y1, int16_t x2, int16_t y2);
uint8_t find_nearest_planet_index(const RadarData *radar, const Spaceship *ship);
bool isShipOwnerOfPlanet(const RadarData *radarData, int ship_id);
uint8_t find_farthest_collector(const RadarData *radar_data);
char* fire_command(RadarData *radarData, uint8_t attacker_id, char *cmd);
#endif /* GAME_FUNCTIONS_H_ */