#ifndef SHIP_LOGIC_H_
#define SHIP_LOGIC_H_

#include "game_functions.h" 

char *collector(const RadarData *radarData, uint8_t collector_id, char *cmd);
char *explorer(const RadarData *radarData, uint8_t explorer_id, uint8_t collector_id, char *cmd);
char *attacker_voyager(const RadarData *radarData, uint8_t attacker_id, char *cmd);
char *attacker(const RadarData *radarData,uint8_t collector_id, uint8_t attacker_id, char *cmd);

#endif /* SHIP_LOGIC_H_ */