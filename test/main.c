#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "minunit.h"

#include "../include/ship_logic.h"
#include "../include/game_functions.h"

//"P 18784 11000 8000 -1 0,P 4320 9000 10000 -1 0,P 23660 15000 14000 -1 0,P 7994 6000 6000 -1 0,P 40094 8000 3000 -1 0,S 0 1 10000 0 0,S 0 2 11500 0 0,S 0 3 8500 0 0,S 0 4 13000 0 0,S 0 5 7000 0 0,S 0 6 14500 0 0,S 0 7 5500 0 0,S 0 8 16000 0 0,S 0 9 4000 0 0,B 10000 0"

MU_TEST(test_atoi_positif)
{
    mu_assert_int_eq(atoi("123"), 123);
    mu_assert_int_eq(atoi("100"), 100);
}

MU_TEST(test_atoi_negatif)
{
    mu_assert_int_eq(atoi("-123"), -123);
    mu_assert_int_eq(atoi("-12345"), -12345);
}

MU_TEST(test_parse_data_planets)
{
    // const char *data = "P 18784 11000 8000 -1 0,P 4320 9000 10000 -1 0,P 23660 15000 14000 -1 0,P 7994 6000 6000 -1 0,P 40094 8000 3000 -1 0,S 0 1 10000 0 0,S 0 2 11500 0 0,S 0 3 8500 0 0,S 0 4 13000 0 0,S 0 5 7000 0 0,S 0 6 14500 0 0,S 0 7 5500 0 0,S 0 8 16000 0 0,S 0 9 4000 0 0,B 10000 0";
    const char *data = "P 18784 11000 8000 -1 0,P 4320 9000 10000 -1 0,S 0 1 10000 0 0,S 0 2 20000 0 0,S 1 1 30000 0 0,S 1 2 40000 0 0,S 2 1 50000 0 0,S 2 4 60000 0 0,S 4 1 30000 0 0,S 4 4 40000 0 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);
    mu_assert_int_eq(18784, radarData.planets[0].planet_id);
    mu_assert_int_eq(4320, radarData.planets[1].planet_id);

    mu_assert_int_eq(11000, radarData.planets[0].abscissa);
    mu_assert_int_eq(9000, radarData.planets[1].abscissa);

    mu_assert_int_eq(8000, radarData.planets[0].ordinate);
    mu_assert_int_eq(10000, radarData.planets[1].ordinate);

    mu_assert_int_eq(-1, radarData.planets[0].ship_id);
    mu_assert_int_eq(-1, radarData.planets[1].ship_id);

    mu_assert_int_eq(0, radarData.planets[0].saved);
    mu_assert_int_eq(0, radarData.planets[1].saved);
}

MU_TEST(test_parse_data_base_station)
{
    const char *data = "P 18784 11000 8000 -1 0,P 4320 9000 10000 -1 0,S 0 2 10000 100 1,S 0 1 20000 200 0,S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 10000 100 1,S 2 3 20000 200 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);

    mu_assert_int_eq(10000, radarData.base_station.abscissa);
    mu_assert_int_eq(0, radarData.base_station.ordinate);
}

MU_TEST(test_parse_my_spaceship)
{
    // const char *data = "S 0 2 10000 100 1,S 0 1 20000 200 0,";
    const char *data = "P 18784 11000 8000 -1 0,P 4320 9000 10000 -1 0,S 0 2 10000 100 1,S 0 1 20000 200 0,S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 10000 100 1,S 2 3 20000 200 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);

    mu_assert_int_eq(0, radarData.my_spaceships[0].team);
    mu_assert_int_eq(1, radarData.my_spaceships[0].ship_id);
    mu_assert_int_eq(20000, radarData.my_spaceships[0].abscissa);
    mu_assert_int_eq(200, radarData.my_spaceships[0].ordinate);
    mu_assert_int_eq(0, radarData.my_spaceships[0].broken);

    mu_assert_int_eq(0, radarData.my_spaceships[1].team);
    mu_assert_int_eq(2, radarData.my_spaceships[1].ship_id);
    mu_assert_int_eq(10000, radarData.my_spaceships[1].abscissa);
    mu_assert_int_eq(100, radarData.my_spaceships[1].ordinate);
    mu_assert_int_eq(1, radarData.my_spaceships[1].broken);
}

MU_TEST(test_parse_enemy_spaceships)
{
    // const char *data = "S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 10000 100 1,S 2 3 20000 200 0";
    const char *data = "P 18784 11000 8000 -1 0,P 4320 9000 10000 -1 0,S 0 2 10000 100 1,S 0 1 20000 200 0,S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 12000 1200 1,S 3 9 20000 200 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);

    mu_assert_int_eq(1, radarData.enemy_spaceships[0].team);
    mu_assert_int_eq(1, radarData.enemy_spaceships[0].ship_id);
    mu_assert_int_eq(20000, radarData.enemy_spaceships[0].abscissa);
    mu_assert_int_eq(200, radarData.enemy_spaceships[0].ordinate);
    mu_assert_int_eq(0, radarData.enemy_spaceships[0].broken);

    mu_assert_int_eq(1, radarData.enemy_spaceships[1].team);
    mu_assert_int_eq(2, radarData.enemy_spaceships[1].ship_id);
    mu_assert_int_eq(10000, radarData.enemy_spaceships[1].abscissa);
    mu_assert_int_eq(100, radarData.enemy_spaceships[1].ordinate);
    mu_assert_int_eq(1, radarData.enemy_spaceships[1].broken);

    mu_assert_int_eq(2, radarData.enemy_spaceships[14].team);
    mu_assert_int_eq(6, radarData.enemy_spaceships[14].ship_id);
    mu_assert_int_eq(12000, radarData.enemy_spaceships[14].abscissa);
    mu_assert_int_eq(1200, radarData.enemy_spaceships[14].ordinate);
    mu_assert_int_eq(1, radarData.enemy_spaceships[14].broken);

    mu_assert_int_eq(3, radarData.enemy_spaceships[26].team);
    mu_assert_int_eq(9, radarData.enemy_spaceships[26].ship_id);
    mu_assert_int_eq(20000, radarData.enemy_spaceships[26].abscissa);
    mu_assert_int_eq(200, radarData.enemy_spaceships[26].ordinate);
    mu_assert_int_eq(0, radarData.enemy_spaceships[26].broken);
}

MU_TEST(test_calcul_angle)
{
    mu_assert_int_eq(45, calcul_angle(0, 0, 1, 1));
    mu_assert_int_eq(90, calcul_angle(0, 0, 0, 1));
    mu_assert_int_eq(0, calcul_angle(0, 0, 1, 0));
    mu_assert_int_eq(180, calcul_angle(1, 1, 0, 1));
    mu_assert_int_eq(331, calcul_angle(2000, 12000, 13000, 6000));
    mu_assert_int_eq(151, calcul_angle(13000, 6000, 2000, 12000));
}

MU_TEST(test_calcul_distance)
{
    mu_assert_int_eq(1, calcul_angle(0, 0, 1, 0));
    mu_assert_int_eq(1, calcul_angle(0, 0, 1, 1));
    mu_assert_int_eq(5000, calcul_angle(10000, 5000, 5000, 5000));
    mu_assert_int_eq(0, calcul_angle(20000, 20000, 20000, 20000));
    mu_assert_int_eq(20000, calcul_angle(0, 20000, 20000, 20000));
}

MU_TEST(test_find_nearest_planet)
{
    const char *data = "P 4320 10 10 -1 0,P 4320 10000 10000 -1 0,P 18784 5 5 -1 1,S 0 1 0 0 0,S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 10000 100 1,S 2 3 20000 200 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);

    uint8_t nearest_planet_index = find_nearest_planet_index(&radarData, &radarData.my_spaceships[0]);

    mu_assert_int_eq(0, nearest_planet_index);
}

MU_TEST(test_isShipOwnerOfPlanet)
{
    const char *data = "P 4320 10 10 1 0,P 4320 10000 10000 -1 0,P 18784 5 5 -1 1,S 0 1 0 0 0,S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 10000 100 1,S 2 3 20000 200 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);


    mu_assert_int_eq(1, isShipOwnerOfPlanet(&radarData, 1));
    mu_assert_int_eq(0, isShipOwnerOfPlanet(&radarData, 2));
}

MU_TEST(test_collector) {
    const char *data = "P 4320 10 10 1 0,P 4320 10000 10000 -1 0,P 18784 5 5 -1 1,S 0 8 0 0 1,S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 10000 100 1,S 2 3 20000 200 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);

    char cmd[20];
    char *result = collector(&radarData, 8, cmd);

    mu_assert_string_eq(result, "MOVE 8 0 1000\n");
}

MU_TEST(test_explorer) {
    const char *data = "P 47372 16000 7000 -1 0,P 18836 15000 12000 -1 0,S 0 1 10000 0 0,S 0 2 11500 0 0,S 0 3 8500 0 0,S 0 4 13000 0 0,S 0 5 7000 0 0,S 0 6 14832 0 0,S 0 7 5035 13 0,S 0 8 16000 99 0,S 0 9 4201 116 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);

    char cmd[20];
    char *result = explorer(&radarData, 7, 9, cmd);

    mu_assert_string_eq(result, "MOVE 7 172 0\n");
}

MU_TEST(test_find_farthest_collector){
    const char *data = "S 0 1 10000 0 0,S 0 2 11500 0 0,S 0 3 8500 0 0,S 0 4 13000 0 0,S 0 5 7000 0 0,S 0 6 14832 0 0,S 0 7 5035 13 0,S 0 8 16000 99 0,S 0 9 4201 116 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);
    mu_assert_int_eq(find_farthest_collector(&radarData), 8);

    data = "S 0 1 10000 0 0,S 0 2 11500 0 0,S 0 3 8500 0 0,S 0 4 13000 0 0,S 0 5 7000 0 0,S 0 6 14832 0 0,S 0 7 5035 13 0,S 0 8 4200 116 0,S 0 9 16000 99 0,B 10000 0";
    parse_data(data, &radarData);
    mu_assert_int_eq(find_farthest_collector(&radarData), 9);
}

MU_TEST(test_fire_command_with_enemy_within_range) {
    const char *data = "S 0 1 0 0 0,S 1 2 1 1 1,S 1 1 0 1 0";
    RadarData radarData;
    parse_data(data, &radarData);
    char cmd[32];

    char* result = fire_command(&radarData, 1, cmd);

    mu_assert_string_eq("FIRE 1 90\n", result);
}

MU_TEST(test_fire_command_without_enemy_within_range) {
    RadarData radarData;
    memset(&radarData, 0, sizeof(RadarData));
    char cmd[32];
    
    char* result = fire_command(&radarData, 1, cmd);

    mu_assert_string_eq("", result);
}

MU_TEST_SUITE(test_suite_atoi)
{
    MU_RUN_TEST(test_atoi_positif);
    MU_RUN_TEST(test_atoi_negatif);
}

MU_TEST_SUITE(test_suite_parse_data)
{
    MU_RUN_TEST(test_parse_data_planets);
    MU_RUN_TEST(test_parse_data_base_station);
    MU_RUN_TEST(test_parse_my_spaceship);
    MU_RUN_TEST(test_parse_enemy_spaceships);
}

MU_TEST_SUITE(test_suite_angle)
{
    MU_RUN_TEST(test_calcul_angle);
}

MU_TEST_SUITE(test_suite_test_distance)
{
    MU_RUN_SUITE(test_calcul_distance);
}

MU_TEST_SUITE(test_suite_find_nearest_planet)
{
    MU_RUN_TEST(test_find_nearest_planet);
}

MU_TEST_SUITE(test_suite_test_isShipOwnerOfPlanet)
{
    MU_RUN_TEST(test_isShipOwnerOfPlanet);
}

MU_TEST_SUITE(test_suite_collector)
{
    MU_RUN_TEST(test_collector);
}

MU_TEST_SUITE(test_suite_explorer)
{
    MU_RUN_TEST(test_explorer);
}

MU_TEST_SUITE(test_suite_find_farthest_collector)
{
    MU_RUN_TEST(test_find_farthest_collector);
}

MU_TEST_SUITE(test_suite_fire) {
    MU_RUN_TEST(test_fire_command_with_enemy_within_range);
    MU_RUN_TEST(test_fire_command_without_enemy_within_range);
}

int main(int argc, char *argv[])
{
    MU_RUN_SUITE(test_suite_atoi);
    MU_RUN_SUITE(test_suite_parse_data);
    MU_RUN_SUITE(test_suite_angle);
    MU_RUN_SUITE(test_suite_test_distance);
    MU_RUN_SUITE(test_suite_find_nearest_planet);
    MU_RUN_SUITE(test_suite_test_isShipOwnerOfPlanet);
    MU_RUN_SUITE(test_suite_collector);
    MU_RUN_SUITE(test_suite_explorer);
    MU_RUN_SUITE(test_suite_find_farthest_collector);
    MU_RUN_SUITE(test_suite_fire);
    MU_REPORT();
    return MU_EXIT_CODE;
}