# SpaceCollector


## Rappel des consignes

Le jeu se déroule sur un terrain carré de 20 000 x 20 000 kilomètres.
Un nombre aléatoire de planètes (entre 2 et 8) est placé sur la carte, avec des positions aléatoires, mais symétriques pour chaque équipe.
Chaque équipe doit collecter ses propres planètes à l'aide de collecteurs, attaquer les ennemis avec des attaquants et explorer avec des explorateurs.
Les vitesses des vaisseaux varient : lente pour les collecteurs, normale pour les explorateurs, et rapide pour les attaquants.
Les attaques à haute énergie ont une portée de moins de 5 000 kilomètres et nécessitent de choisir un angle.
Si un vaisseau est touché par une attaque à haute énergie, il doit retourner à sa base pour être réparé.
Les attaquants ne peuvent pas attaquer lorsqu'ils sont endommagés, et les explorateurs ne peuvent pas utiliser leur radar pour détecter les vaisseaux ennemis.
La collecte des planètes cesse lorsque chaque équipe a collecté toutes ses planètes ou lorsque le jeu atteint 5 minutes. Pour plus d'informations: [vpoulailleau space_collector](https://github.com/vpoulailleau/space_collector)

**Commandes :**

- `MOVE {ship_id} {angle} {speed}` : Change la vitesse et l'angle du vaisseau.
- `FIRE {ship_id} {angle}` : Déclenche une attaque à haute énergie dans une direction donnée.
- `RADAR {ship_id}` : Active le radar d'un explorateur pour détecter les planètes et les vaisseaux ennemis à proximité.

Chaque commande renvoie une réponse indiquant le succès de l'action ou des informations pertinentes concernant l'état du jeu et des vaisseaux.


## Telecharger et lancer le projet

### Telecharger le projet
```
cd you folder
git clone https://gitlab.com/.../spacecollector.git

```

### Lancer le jeux

#### Lancer le serveur avec un joueur

créer un dossier
```
mkdir competition_server.sh
```
ajouter: (modifier IP, PORT, PORT SERIE)
```
PORT=12345
IP="192.168.1.56"
echo "PORT $PORT"
SERIAL=/dev/ttyUSB0
python  -m space_collector.killall.py
find . -name "*.log" -exec rm \{} \;

python -m space_collector.game.server -a $IP -p $PORT --timeout 10 &
python -m space_collector.viewer -a $IP -p $PORT &
sleep 2
python -m space_collector.serial2tcp  -p $PORT --serial $SERIAL --team-name "team" -a $IP
sleep 3300
python -m space_collector.killall

```

```
chmod +x competition_server.sh

```
puis
```
./competition.sh
```
#### Pour créer un client

créer un dossier
```
mkdir competition_client.sh
```
ajouter: (modifier IP, PORT, PORT SERIE)

```
PORT=$(python -c "import random; print(random.randint(50000, 60000))")
echo "La compétition se passe sur 127.0.0.1:$PORT"
SERIAL=/dev/tty.usbserial-A703FW3Y
PORT=12346

python  -m space_collector.killall.py
find . -name "*.log" -exec rm \{} \

python -m space_collector.serial2tcp  -p $PORT --serial $SERIAL --team-name "Wild Boar" -a 192.168.1.56

sleep 330
python -m space_collector.killall
```
ensuite faire :
```
chmod +x competition_client.sh
```
puis

```
 ./competition_client.sh
```


## Démarche mise en place

- [ ] [Création d'un repository gitlab](https://docs.gitlab.com/ee/user/project/repository/)
- [ ] [Mise en place d'une CI sur gitlab (Build, Rules et Test)](#mise-en-place-ci)
- [ ] [Réflexion sur la stratégie à choisir](#reflexion-sur-la-strategie-a-choisir)
- [ ] [Mise en place des différents tests unitaires](#mise-en-place-des-differents-tests-unitaires ) 

## 3 plus grandes difficultés et solutions mises en place

- [ ] [difficultés 1: decouverte de CMSIS-RTOS2](#difficulte-1-:-decouverte-de-CMSIS-RTOS2)
- [ ] [difficultés 2: le parssing des data reçu](#difficulte-2-:-le-parssing-des-data-reçu)
- [ ] [difficultés 3: ralentissement du jeux](#difficulte-3-:-ralentissement-du-jeux)

## Notions apprises

Durant ce projet nous avons acquit les connaissances suivantes :
- Écriture des tests unitaires avec la librairie munuit.h
- Mise en place de mutex pour gérer et prioriser les différents threads dans notre code
- Utilisation et mise en place de CMSIS-RTOS2




## Retour d'expérience

Nous avons trouver intéressant le fait de devoir participer à un jeu afin de mettre en application les connaissances théorique acquise durant les cours. 

Cependant il aurait ete aprécier d'avoir quelque heure de "formation" sur CMSIS-RTOS2 avec quelque exemple afin de gagner du temps et surtout afin de mieux comprendre comme intégrer la bibliothèque a notre code.

***

# Mise en place CI 

## Annexes creation d'une CI 
- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
- [ ] [Code Quality](https://repository.prace-ri.eu/git/help/ci/testing/code_quality.md)

***

# Reflexion sur la strategie a choisir

Notre stratégie consiste à avoir deux collecteurs cherchant la planète la plus proche, de sorte que si l'un d'eux est détruit, l'autre peut récupérer la planète. Les radars suivent en permanence les deux collecteurs. Quant aux attaquants, nous les avons répartis avec un attaquant par collecteur et les trois autres se dirigent vers le collecteur le plus éloigné. En cas de destruction de n'importe quel vaisseau de la flotte, il est ordonné de retourner à la base pour être réparé et reprendre la tâche précédente après sa réparation.


***

# Mise en place des differents tests unitaires 
Nous avons mis en place des tests unitaires pour vérifier le bon fonctionnement de notre code. Par exemple, dans un test, nous vérifions si notre fonction de calcul d'angle renvoie la bonne valeur en lui passant deux positions.

Dans le premier nous testons si notre fonction calcul_angle nous retourne la bonne valeur en luis passant 2 positions.
```c
MU_TEST(test_calcul_angle)
{
    mu_assert_int_eq(45, calcul_angle(0, 0, 1, 1));
    mu_assert_int_eq(90, calcul_angle(0, 0, 0, 1));
    mu_assert_int_eq(0, calcul_angle(0, 0, 1, 0));
    mu_assert_int_eq(180, calcul_angle(1, 1, 0, 1));
    mu_assert_int_eq(331, calcul_angle(2000, 12000, 13000, 6000));
    mu_assert_int_eq(151, calcul_angle(13000, 6000, 2000, 12000));
}

```
Dans le second on test si la fonction parse_data ecrit dans les bonnes variables l'abscisse et l'ordonée de la base.
```c
MU_TEST(test_parse_data_base_station)
{
    const char *data = "P 18784 11000 8000 -1 0,P 4320 9000 10000 -1 0,S 0 2 10000 100 1,S 0 1 20000 200 0,S 1 2 10000 100 1,S 1 1 20000 200 0,S 2 6 10000 100 1,S 2 3 20000 200 0,B 10000 0";
    RadarData radarData;
    parse_data(data, &radarData);

    mu_assert_int_eq(10000, radarData.base_station.abscissa);
    mu_assert_int_eq(0, radarData.base_station.ordinate);
}

```

***

# Difficulte 1 : decouverte de CMSIS-RTOS2
La découverte de [CMSIS-RTOS2](https://arm-software.github.io/CMSIS_6/latest/RTOS2/group__CMSIS__RTOS.html) fut pour nous un probleme de taille car cela impliquait de consacrer du temps pour se former sur cette façon de coder sur microcontrolleur.

## Solution 1
Nous avons pris le temps d'étudier la documentation de CMSIS-RTOS2 et avons réalisé des exemples pratiques pour comprendre son fonctionnement. Nous avons également recherché des ressources en ligne et échangé avec d'autres développeurs expérimentés pour obtenir des conseils et des astuces.

***

# Difficulte 2 : le parssing des data reçu
Durant le devellopement du jeux nous avons tout dabord rencontrer des difficultés a exploiter les données envoyer par le serveur. 

```
P 32596 127 9891 -1 1,P 2888 160 10098 -1 1,P 18042 180 10084 -1 1,P 18872 3412 11321 9 0,S 0 1 429 4750 0,S 0 2 0 8500 0,S 0 3 0 11500 0,S 0 4 0 7000 0,S 0 5 4850 11632 0,S 0 6 532 3943 0,S 0 7 4677 11823 0,S 0 8 590 3213 0,S 0 9 3412 11321 0,B 0 10000,S 1 7 3173 12140 0,S 1 9 1585 13169 0

```

## Solution 2
Nous avons développé des fonctions de parsing robustes en utilisant des bibliothèques ou des fonctions intégrées au langage de programmation que nous utilisons. Nous avons également effectué des tests unitaires pour vérifier la précision et la fiabilité du parsing des données.

***

# Difficulte 3 : ralentissement du jeux
Nous avions envisagé, suite a différents test la possibilité que notre jeu soit ralenti a cause des différents mutex implanté dans notre code.

## Solution 3
Nous avons identifié les causes potentielles de ralentissement du jeu, telles que des algorithmes inefficaces, une utilisation excessive des ressources système ou des erreurs de gestion de la mémoire. Ensuite, nous avons optimisé le code en réécrivant les parties critiques, en évitant les opérations coûteuses et en limitant la consommation des ressources système.

En ce qui concerne les attaquants n'ayant pas été designer comme attaquant principale, ils se déplacent vers le collecteur qui est le plus loin de la base (déjà implementé).  

***
## Étape du projet

Pour ce projet, nous avons commencer par choisir une solution pour organiser les données provenant du serveur, ce qui nous a conduit à élaborer une structure de données. Ensuite, nous avons mis en place une fonction permettant de découper une chaîne de caractères et d'insérer les morceaux dans cette structure. Une fois cette étape franchie, nous avons voulu déplacer un collecteur. Pour ce faire, nous avons d'abord eu besoin d'une fonction retournant l'angle de direction souhaité, suivie d'une autre fonction pour déterminer la distance et ainsi identifier la planète la plus proche.

Une fois le fonctionnement du collecteur assuré, nous avons développé une fonction pour déplacer le radar, puis une autre pour gérer les attaquants. En ce qui concerne ces derniers, nous avons élaboré une fonction destinée à rechercher les ennemis dans la chaîne de caractères, fournissant ainsi l'angle de tir.

Dans le fichier Main.c, nous avions initialement tenté d'exécuter nos threads en utilisant des flags, mais cela s'est avéré ne pas être la solution optimale. Nous avons rencontré des difficultés à utiliser les threads, et avons réalisé que la taille de notre pile était inappropriée. Lorsque nous avons identifié ce problème, nous avons retravaillé l'intégralité du Main.c pour ajuster la taille des piles, afin de résoudre ces problèmes.

Pour chaque fonction que nous avons créée, nous avons d'abord rédigé les tests, puis développé la fonction elle-même. Une fois que les tests ont été validés en local, nous avons ajouté les nouvelles portions de code à notre dépôt GitLab.



## Résultat

Dans la vidéo, l'équipe représentée en jaune représente notre stratégie actuelle.

![ ](Ressources/Capture_video_du_12-05-2024_183428.mp4)