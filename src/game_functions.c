// This file contains the functions that will be used for the game
#include "../include/game_functions.h"

int atoi(const char *str)
{
    int result = 0;
    int sign = 1;

    if (*str == '-')
    {
        sign = -1;
        str++;
    }
    else if (*str == '+')
    {
        str++;
    }

    while (*str >= '0' && *str <= '9')
    {
        result = result * 10 + (*str - '0');
        str++;
    }

    return sign * result;
}

void parse_data(const char *data, RadarData *radarData)
{
    int planet_index = 0;

    while (*data != '\0')
    {
        char type;
        int abscissa, ordinate;
        int result = sscanf(data, "%c %d %d", &type, &abscissa, &ordinate);

        if (result == EOF || result < 3)
        {
            break;
        }

        switch (type)
        {
        case 'P':
        {
            unsigned short planet_id;
            int ship_id, saved;
            result = sscanf(data, "P %hu %d %d %d %d", &planet_id, &abscissa, &ordinate, &ship_id, &saved);
            if (result == 5)
            {
                radarData->planets[planet_index++] = (Planet){planet_id, abscissa, ordinate, ship_id, saved};
            }
            break;
        }
        case 'S':
        {
            int team, ship_id, broken;
            result = sscanf(data, "S %d %d %d %d %d", &team, &ship_id, &abscissa, &ordinate, &broken);
            if (result == 5)
            {
                Spaceship spaceship = {team, ship_id, abscissa, ordinate, broken};
                if (team == 0)
                {
                    radarData->my_spaceships[ship_id - 1] = spaceship;
                }
                else if (team >= 1 && team <= 3)
                {
                    radarData->enemy_spaceships[((team - 1) * MAX_SPACESHIPS ) + ship_id - 1] = spaceship;
                }
            }
            break;
        }
        case 'B':
            sscanf(data, "B %hu %hu", &radarData->base_station.abscissa, &radarData->base_station.ordinate);
            break;
        default:
            break;
        }

        data = strchr(data, ',');
        if (data == NULL)
        {
            break;
        }
        data++;
    }
}

uint16_t calcul_angle(int16_t x1, int16_t y1, int16_t x2, int16_t y2)
{

    int16_t dx = x2 - x1;
    int16_t dy = y2 - y1;

    double angle_rad = atan2(dy, dx);

    double angle_deg = angle_rad * (180.0 / M_PI);

    if (angle_deg < 0)
    {
        angle_deg += 360.0;
    }
    return (uint16_t)angle_deg;
}

uint16_t calcul_distance(int16_t x1, int16_t y1, int16_t x2, int16_t y2)
{
    int16_t dx = x2 - x1;
    int16_t dy = y2 - y1;
    return (uint16_t)round(sqrt(dx * dx + dy * dy));
}

uint8_t find_nearest_planet_index(const RadarData *radar, const Spaceship *ship)
{
    uint8_t nearest_planet_index = -1;
    uint16_t min_distance = 65535; 

    for (int i = 0; i < MAX_PLANETS; ++i)
    {
        if (radar->planets[i].planet_id != 0 && radar->planets[i].saved == 0 && radar->planets[i].ship_id == -1)
        {
            uint16_t distance = calcul_distance(ship->abscissa, ship->ordinate,
                                                radar->planets[i].abscissa, radar->planets[i].ordinate);
            if (distance < min_distance)
            {
                min_distance = distance;
                nearest_planet_index = i;
            }
        }
    }

    return nearest_planet_index;
}


bool isShipOwnerOfPlanet(const RadarData *radarData, int ship_id) { 
    for (int i = 0; i < MAX_PLANETS; i++) {
        if (radarData->planets[i].ship_id == ship_id) {
            return true; 
        }
    }
    return false; 
}

uint8_t find_farthest_collector(const RadarData *radar_data)
{
   uint16_t distance_ship_8 = calcul_distance(radar_data->my_spaceships[7].abscissa, radar_data->my_spaceships[7].ordinate,
                                                   radar_data->base_station.abscissa, radar_data->base_station.ordinate);

    uint16_t distance_ship_9 = calcul_distance(radar_data->my_spaceships[8].abscissa, radar_data->my_spaceships[8].ordinate,
                                                   radar_data->base_station.abscissa, radar_data->base_station.ordinate);
    if (distance_ship_8 > distance_ship_9) {
        return 8;
    } else {
        return 9;
    }
}

char* fire_command(RadarData *radarData, uint8_t attacker_id, char *cmd) {
    if (radarData->my_spaceships[attacker_id - 1].broken) {
        snprintf(cmd, 2 * sizeof(char), "");
        return cmd;
    }

    uint16_t attacker_abscissa = radarData->my_spaceships[attacker_id - 1].abscissa;
    uint16_t attacker_ordinate = radarData->my_spaceships[attacker_id - 1].ordinate;

    for (int i = 0; i < 27; i++) {
        if (!radarData->enemy_spaceships[i].broken &&
            radarData->enemy_spaceships[i].ship_id != 0) {

            uint16_t enemy_abscissa = radarData->enemy_spaceships[i].abscissa;
            uint16_t enemy_ordinate = radarData->enemy_spaceships[i].ordinate;

            int32_t dx = attacker_abscissa - enemy_abscissa;
            int32_t dy = attacker_ordinate - enemy_ordinate;
            uint32_t distance_squared = dx * dx + dy * dy;

            if (distance_squared <= 5000 * 5000) {
                uint16_t angle = calcul_angle(attacker_abscissa, attacker_ordinate,
                                              enemy_abscissa, enemy_ordinate);
                snprintf(cmd, 32 * sizeof(char), "FIRE %d %hu\n", attacker_id, angle);
                return cmd;
            }
        }
    }

    snprintf(cmd, 2 * sizeof(char), "");
    return cmd;
}



