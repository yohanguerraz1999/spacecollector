#include "cmsis_os2.h"
#include <stdio.h>
#include "../include/main.h"
#include <string.h>
#include "../include/game_functions.h"
#include "../include/ship_logic.h"
#include "../include/constants.h"


RadarData radar_data;
char buffer[1024];
char cmd[32];


// Déclaration des fonctions utilisées par les threads
void collector_1(void *argument);
void collector_2(void *argument);
void explorer_1(void *argument);
void explorer_2(void *argument);

void attacker_voyager_1(void *argument);
void attacker_voyager_2(void *argument);
void attacker_voyager_3(void *argument);

void attacker_1(void *argument);
void attacker_2(void *argument);


// Déclaration du mutex
osMutexId_t serial_mutex;
const osMutexAttr_t serial_mutex_attr = {
    "SerialMutex",
    osMutexRecursive | osMutexPrioInherit,
    NULL,
    0U
};

// Déclaration des threads
osThreadId_t tid_thread1, tid_thread2, tid_thread3, tid_thread4, tid_thread5, tid_thread6, tid_thread7, tid_thread8, tid_thread9;

const osThreadAttr_t thread_attr1 = {
    .stack_size = 1024,
};

const osThreadAttr_t thread_attr2 = {
    .stack_size = 2048, // Taille de la pile pour chaque thread
};

void collector_1(void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(collector(&radar_data, COLLECTOR_1, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);
        osDelay(DELAY); 
    }
}

void collector_2(void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(collector(&radar_data, COLLECTOR_2, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);
        osDelay(DELAY); 
    }
}

void explorer_1(void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts("RADAR 6\n");
        gets(buffer);
        parse_data(buffer, &radar_data);
        osMutexRelease(serial_mutex);

        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(explorer(&radar_data, EXPLORER_1, COLLECTOR_1, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);
        osDelay(DELAY); 
    }
}

void explorer_2(void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts("RADAR 7\n");
        gets(buffer);
        parse_data(buffer, &radar_data);
        osMutexRelease(serial_mutex);

        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(explorer(&radar_data, EXPLORER_2, COLLECTOR_2, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);
        osDelay(DELAY); 
    }
}

void attacker_voyager_1(void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(attacker_voyager(&radar_data, ATTACKER_2, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);

        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(fire_command(&radar_data, ATTACKER_2, cmd));
        osMutexRelease(serial_mutex);

        osDelay(DELAY); 
    }
}

void attacker_voyager_2(void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(attacker_voyager(&radar_data, ATTACKER_3, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);

        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(fire_command(&radar_data, ATTACKER_3, cmd));
        osMutexRelease(serial_mutex);

        osDelay(DELAY); 
    }
}

void attacker_voyager_3(void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(attacker_voyager(&radar_data, ATTACKER_4, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);

        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(fire_command(&radar_data, ATTACKER_4, cmd));
        osMutexRelease(serial_mutex);

        osDelay(DELAY); 
    }
}

void attacker_1 (void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(attacker(&radar_data, COLLECTOR_1, ATTACKER_1, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);

        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(fire_command(&radar_data, ATTACKER_1, cmd));
        osMutexRelease(serial_mutex);
        
        osDelay(DELAY); 
    }
}

void attacker_2 (void *argument) {
    while (1) {
        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(attacker(&radar_data, COLLECTOR_2, ATTACKER_5, cmd));
        gets(buffer);
        osMutexRelease(serial_mutex);

        osMutexAcquire(serial_mutex, osWaitForever);
        game_puts(fire_command(&radar_data, ATTACKER_5, cmd));
        osMutexRelease(serial_mutex);

        osDelay(DELAY); 
    }
}

int main() {

    hardware_init();

    osKernelInitialize();

    // Création du mutex
    serial_mutex = osMutexNew(&serial_mutex_attr);

    // Création des threads
    tid_thread1 = osThreadNew(collector_1, NULL, &thread_attr1);
    tid_thread2 = osThreadNew(collector_2, NULL, &thread_attr1);

    tid_thread3 = osThreadNew(explorer_1, NULL, &thread_attr1);
    tid_thread4 = osThreadNew(explorer_2, NULL, &thread_attr1);

    tid_thread5 = osThreadNew(attacker_voyager_1, NULL, &thread_attr2);
    tid_thread6 = osThreadNew(attacker_voyager_2, NULL, &thread_attr2);
    tid_thread7 = osThreadNew(attacker_voyager_3, NULL, &thread_attr2);

    tid_thread8 = osThreadNew(attacker_1, NULL, &thread_attr1);
    tid_thread9 = osThreadNew(attacker_2, NULL, &thread_attr1);

    // Démarrage du système d'exploitation temps réel
    // Récupération de start
    char command[20];
    gets(command);

    osKernelStart();

    // On ne devrait jamais atteindre cette ligne
    for (;;);
    return 0;
}
