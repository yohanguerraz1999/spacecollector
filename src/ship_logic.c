#include "../include/ship_logic.h"

char *collector(const RadarData *radarData, uint8_t collector_id, char *cmd)
{
    uint8_t nearest_planet_index = 0;
    uint16_t angle = 0;
    uint16_t speed = 1000;

    nearest_planet_index = find_nearest_planet_index(radarData, &radarData->my_spaceships[collector_id - 1]);

    if(radarData->my_spaceships[collector_id - 1].broken == 0)
    {
        if(isShipOwnerOfPlanet(radarData, collector_id))
        {
            angle = calcul_angle(
                radarData->my_spaceships[collector_id - 1].abscissa,
                radarData->my_spaceships[collector_id - 1].ordinate,
                radarData->base_station.abscissa,
                radarData->base_station.ordinate);
            snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", collector_id, angle, speed);
        }
        else if (radarData->planets[nearest_planet_index].ship_id != collector_id)
        {
            angle = calcul_angle(
                radarData->my_spaceships[collector_id - 1].abscissa,
                radarData->my_spaceships[collector_id - 1].ordinate,
                radarData->planets[nearest_planet_index].abscissa,
                radarData->planets[nearest_planet_index].ordinate);
            snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", collector_id, angle, speed);
        }
    }
    else if(radarData->my_spaceships[collector_id - 1].broken == 1)
    {
        angle = calcul_angle(
            radarData->my_spaceships[collector_id - 1].abscissa,
            radarData->my_spaceships[collector_id - 1].ordinate,
            radarData->base_station.abscissa,
            radarData->base_station.ordinate);
        snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", collector_id, angle, speed);
    }
    return cmd;
}

char *explorer(const RadarData *radarData, uint8_t explorer_id, uint8_t collector_id, char *cmd)
{
    uint16_t angle = 0;
    uint16_t speed = 0;
    uint16_t distance_to_collector = 0;

    if(radarData->my_spaceships[explorer_id - 1].broken == 0)
    {
        angle = calcul_angle(
            radarData->my_spaceships[explorer_id - 1].abscissa,
            radarData->my_spaceships[explorer_id - 1].ordinate,
            radarData->my_spaceships[collector_id - 1].abscissa,
            radarData->my_spaceships[collector_id - 1].ordinate);

        distance_to_collector = calcul_distance(
            radarData->my_spaceships[explorer_id - 1].abscissa,
            radarData->my_spaceships[explorer_id - 1].ordinate,
            radarData->my_spaceships[collector_id - 1].abscissa,
            radarData->my_spaceships[collector_id - 1].ordinate
        );
        if(distance_to_collector > 1000)
        {
            speed = 2000;
        }
        else if(distance_to_collector < 500 && distance_to_collector >= 20)
        {
            speed = 1000;
        }
        else if(distance_to_collector < 20)
        {
            speed = 0;
        }
        snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", explorer_id, angle, speed);
    }
    else if(radarData->my_spaceships[explorer_id - 1].broken == 1)
    {
        speed = 2000;
        angle = calcul_angle(
            radarData->my_spaceships[explorer_id - 1].abscissa,
            radarData->my_spaceships[explorer_id - 1].ordinate,
            radarData->base_station.abscissa,
            radarData->base_station.ordinate);
        snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", explorer_id, angle, speed);
    }
    return cmd;
}


char *attacker_voyager(const RadarData *radarData, uint8_t attacker_id, char *cmd)
{
    uint16_t angle = 0;
    uint16_t speed = 0;
    uint16_t distance_to_collector = 0;

    uint8_t collector_id = 9;

    collector_id = find_farthest_collector(radarData);

    if(radarData->my_spaceships[attacker_id - 1].broken == 0)
    {
        angle = calcul_angle(
            radarData->my_spaceships[attacker_id - 1].abscissa,
            radarData->my_spaceships[attacker_id - 1].ordinate,
            radarData->my_spaceships[collector_id - 1].abscissa,
            radarData->my_spaceships[collector_id - 1].ordinate);

        distance_to_collector = calcul_distance(
            radarData->my_spaceships[attacker_id - 1].abscissa,
            radarData->my_spaceships[attacker_id - 1].ordinate,
            radarData->my_spaceships[collector_id - 1].abscissa,
            radarData->my_spaceships[collector_id - 1].ordinate
        );
        if(distance_to_collector > 1000)
        {
            speed = 3000;
        }
        else if(distance_to_collector < 500 && distance_to_collector >= 20)
        {
            speed = 1000;
        }
        else if(distance_to_collector < 20)
        {
            speed = 0;
        }
        snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", attacker_id, angle, speed);
    }
    else if(radarData->my_spaceships[attacker_id - 1].broken == 1)
    {
        speed = 3000;
        angle = calcul_angle(
            radarData->my_spaceships[attacker_id - 1].abscissa,
            radarData->my_spaceships[attacker_id - 1].ordinate,
            radarData->base_station.abscissa,
            radarData->base_station.ordinate);
        snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", attacker_id, angle, speed);
    }
    return cmd;
}

char *attacker(const RadarData *radarData,uint8_t collector_id, uint8_t attacker_id, char *cmd)
{
    uint16_t angle = 0;
    uint16_t speed = 0;
    uint16_t distance_to_collector = 0;

    if(radarData->my_spaceships[attacker_id - 1].broken == 0)
    {
        angle = calcul_angle(
            radarData->my_spaceships[attacker_id - 1].abscissa,
            radarData->my_spaceships[attacker_id - 1].ordinate,
            radarData->my_spaceships[collector_id - 1].abscissa,
            radarData->my_spaceships[collector_id - 1].ordinate);

        distance_to_collector = calcul_distance(
            radarData->my_spaceships[attacker_id - 1].abscissa,
            radarData->my_spaceships[attacker_id - 1].ordinate,
            radarData->my_spaceships[collector_id - 1].abscissa,
            radarData->my_spaceships[collector_id - 1].ordinate
        );
        if(distance_to_collector > 1000)
        {
            speed = 3000;
        }
        else if(distance_to_collector < 500 && distance_to_collector >= 20)
        {
            speed = 1000;
        }
        else if(distance_to_collector < 20)
        {
            speed = 0;
        }
        snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", attacker_id, angle, speed);
    }
    else if(radarData->my_spaceships[attacker_id - 1].broken == 1)
    {
        speed = 3000;
        angle = calcul_angle(
            radarData->my_spaceships[attacker_id - 1].abscissa,
            radarData->my_spaceships[attacker_id - 1].ordinate,
            radarData->base_station.abscissa,
            radarData->base_station.ordinate);
        snprintf(cmd, 32 * sizeof(char), "MOVE %d %hu %hu\n", attacker_id, angle, speed);
    }
    return cmd;
}